package com.dane.encuesta.service;

import com.dane.encuesta.model.Expenses;
import com.dane.encuesta.repository.ExpensesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpensesServiceImpl implements ExpensesService {
    @Autowired
    private ExpensesRepository expensesRepository;

//    public ExpensesServiceImpl(ExpensesRepository expensesRepository) {
//        this.expensesRepository = expensesRepository;
//    }

    @Override
    public Expenses saveExpenses(Expenses expenses) {
        return expensesRepository.save(expenses);
    }

    @Override
    public void deleteExpenses(Integer expensesId) {
        expensesRepository.deleteById(expensesId);
    }

    @Override
    public List<Expenses> findAllExpenses() {
        return expensesRepository.findAll();
    }

}
