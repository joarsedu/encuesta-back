package com.dane.encuesta.service;

import com.dane.encuesta.model.User;
import java.util.List;

public interface UserService {
    User saveUser(User user);

    void deleteUser(Integer userId);

    List<User> findAllUser();
}
