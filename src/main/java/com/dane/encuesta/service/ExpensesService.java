package com.dane.encuesta.service;

import com.dane.encuesta.model.Expenses;

import java.util.List;

public interface ExpensesService {

    Expenses saveExpenses(Expenses expenses);

    void deleteExpenses(Integer expensesId);

    List<Expenses> findAllExpenses();
}
