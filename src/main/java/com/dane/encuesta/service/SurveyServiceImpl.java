package com.dane.encuesta.service;

import com.dane.encuesta.model.Survey;
import com.dane.encuesta.repository.SurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SurveyServiceImpl implements SurveyService {
    @Autowired
    private SurveyRepository surveyRepository;

    @Override
    public Survey saveSurvey(Survey survey) {
        return surveyRepository.save(survey);
    }

    @Override
    public void deleteSurvey(Integer surveyId) {
        surveyRepository.deleteById(surveyId);
    }

    @Override
    public List<Survey> findAllSurvey() {
        return surveyRepository.findAll();
    }

}
