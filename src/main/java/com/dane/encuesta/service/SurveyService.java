package com.dane.encuesta.service;

import com.dane.encuesta.model.Survey;

import java.util.List;

public interface SurveyService {
    Survey saveSurvey(Survey survey);

    void deleteSurvey(Integer surveyId);

    List<Survey> findAllSurvey();
}
