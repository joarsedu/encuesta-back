package com.dane.encuesta.controller;

import com.dane.encuesta.model.Survey;
import com.dane.encuesta.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/survey")
public class SurveyController {
    @Autowired
    private SurveyService surveyService;

    // method POST
    // http://localhost/api/survey
    @PostMapping
    public ResponseEntity<?> saveSurvey(@RequestBody Survey survey) {
        return new ResponseEntity<>(
                surveyService.saveSurvey(survey),
                HttpStatus.CREATED);
    }

    // method DELETE
    // http://localhost/api/survey/1145
    @DeleteMapping("{surveyId}")
    public ResponseEntity<?> deleteSurvey(@PathVariable Integer surveyId) {
        surveyService.deleteSurvey(surveyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // method GET
    // http://localhost/api/survey
    @GetMapping
    public ResponseEntity<?> getAllSurveys() {
        return new ResponseEntity<>(
                surveyService.findAllSurvey(),
                HttpStatus.CREATED);
    }
}
