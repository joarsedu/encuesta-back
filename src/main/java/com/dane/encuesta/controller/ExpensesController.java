package com.dane.encuesta.controller;

import com.dane.encuesta.model.Expenses;
import com.dane.encuesta.service.ExpensesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/expenses")
public class ExpensesController {
    @Autowired
    private ExpensesService expensesService;

    // @GetMapping("/")
    // public String index() {
    //     return "Greetings from Spring Boot!";
    // }

    // method POST
    // http://localhost/api/expenses
    @PostMapping
    public ResponseEntity<?> saveExpenses(@RequestBody Expenses expenses) {
        return new ResponseEntity<>(
                expensesService.saveExpenses(expenses),
                HttpStatus.CREATED);
    }

    // method DELETE
    // http://localhost/api/expenses/1145
    @DeleteMapping("{expensesId}")
    public ResponseEntity<?> deleteExpenses(@PathVariable Integer expensesId) {
        expensesService.deleteExpenses(expensesId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // method GET
    // http://localhost/api/expenses
    @GetMapping
    public ResponseEntity<?> getAllExpensess() {
        return new ResponseEntity<>(
                expensesService.findAllExpenses(),
                HttpStatus.CREATED);
    }

}
