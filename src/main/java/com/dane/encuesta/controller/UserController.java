package com.dane.encuesta.controller;

import com.dane.encuesta.model.User;
import com.dane.encuesta.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    // method POST
    // http://localhost/api/user
    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody User user) {
        return new ResponseEntity<>(
                userService.saveUser(user),
                HttpStatus.CREATED);
    }

    // method DELETE
    // http://localhost/api/user/1145
    @DeleteMapping("{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Integer userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // method GET
    // http://localhost/api/user
    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        return new ResponseEntity<>(
                userService.findAllUser(),
                HttpStatus.CREATED);
    }
}
