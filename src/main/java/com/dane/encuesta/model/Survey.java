package com.dane.encuesta.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "survey")

public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_user")
//    private User idUser;

    @Column(name = "id_user", length = 11)
    private int id_user;

    @Column(name = "country", length = 11)
    private String country;

    @Column(name = "nationality", length = 11)
    private String nationality;

    @Column(name = "sex", length = 11)
    private String sex;

    @Column(name = "age", length = 11)
    private int age;

    @Column(name = "with_whom_do_you_travel", length = 11)
    private String with_whom_do_you_travel;

    @Column(name = "with_whom_do_you_travel_other", length = 11)
    private String with_whom_do_you_travel_other;

    @Column(name = "with_how_many_people", length = 11)
    private int with_how_many_people;

    @Column(name = "reason_for_travel", length = 11)
    private String reason_for_travel;

    @Column(name = "reason_for_travel_other", length = 11)
    private String reason_for_travel_other;

    @Column(name = "how_organized_travel", length = 11)
    private String how_organized_travel;

    @Column(name = "how_organized_travel_other", length = 11)
    private String how_organized_travel_other;

    @Column(name = "travel_services", length = 11)
    private String travel_services;

    @Column(name = "travel_services_other", length = 11)
    private String travel_services_other;
}
