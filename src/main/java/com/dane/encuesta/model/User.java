package com.dane.encuesta.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name="user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "gender", length = 1)
    private String gender;

    @Column(name = "age")
    private int age;

//    @OneToOne(mappedBy = "idUser")
//    private Survey survey;

}
