package com.dane.encuesta.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "expenses")
public class Expenses {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_survey", length = 11)
    private int id_survey;

    @Column(name = "expense_name", length = 150)
    private String expense_name;

    @Column(name = "there_was_expense", length = 2)
    private String there_was_expense;

    @Column(name = "paid_by_you", length = 11)
    private int paid_by_you;

    @Column(name = "paid_by_you_currency", length = 10)
    private String paid_by_you_currency;

    @Column(name = "third_parties_not", length = 11)
    private int third_parties_not;

    @Column(name = " third_parties_not_currency", length = 10)
    private String  third_parties_not_currency ;

    @Column(name = "third_parties_yes", length = 11)
    private int third_parties_yes;

    @Column(name = "third_parties_yes_currency", length = 10)
    private String third_parties_yes_currency;

    @Column(name = "how_many_people", length = 11)
    private int how_many_people;
}
