package com.dane.encuesta.repository;

import com.dane.encuesta.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
